---
title: Lęgi
---

5 jaj
1 złożone 30 maja
foto

3 rozgniecione i znalezione na zewnątrz budki
1 niezapłodnione

1 pisklę wyklute 


Samica halaśliwie krzyczy z budki wieczorem przy pracach które poruszają całą klatką i szeleszczą wokół budki.

11 lipca, pisklę się dołączyło do tych hałasów.

Samiec śpi u frontu budki, w bezpiecznym miejscu ale jak najbliżej. Wcześniej inaczej spał, raz tu raz tam.

Kopulacja, w dniu wylotu pisklęcia z gniazda.

25 lipca, pisklę wylatuje z gniazda. Kopulacja pary.
26 lipca, 1 jajo.
28 lipca, 2 jajo. Samica robi dość długie przerwy w wysiadywaniu.
30 lipca, 3 jajo. Samica siedzi już w budce cały czas a samiec ją goni jak jest na zewnątrz.
1 sierpnia, 4 jajo. Kopulacja, kopulacje co drugi dzień?
3 sierpnia, 5 jajo.
5 sierpnia, 6 jajo.
7 sierpnia, 7 jajo.
9 sierpnia, 8 jajo.

13 sierpnia, 1 piskle się wykluło (i jest jeszcze 8 jaj, więc było 9 w sumie).
